import React from 'react';

import LeaveRequest from './LeaveRequest'
import ManageLeaveForm from './ManageLeaveForm'

import { Table, Button, Form, FormGroup, Label, Input, FormText, Jumbotron } from 'reactstrap';
import { InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';


const API_URL = process.env.REACT_APP_API_URL;

class ManagerPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            managedLeave: undefined,
            leaveRequests : []
        }
        this.refresh = this.refresh.bind(this);
        this.rejectLeave = this.rejectLeave.bind(this);
        this.approveLeave = this.approveLeave.bind(this);
        this.viewLeave = this.viewLeave.bind(this);
        this.back = this.back.bind(this);
    }

    refresh() {
        fetch(API_URL + '/manager')
        .then(res => res.json())
        .then((data) => {
            this.setState({
                leaveRequests: data,
                managedLeave: undefined
            });
        });
    }

    componentDidMount() {
        this.refresh();
    }

    approveLeave(request) {
        fetch(API_URL + '/manager',
        {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json' 
            },
            body: JSON.stringify({
                leaveId: request.id,
                status: 'Approved',
                updateComment: 'Approved'
            })
        }).then(() => {
            this.refresh();
        });
    }

    rejectLeave(request, reason) {
        fetch(API_URL + '/manager', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                leaveId: request.id,
                status: 'Rejected',
                updateComment: reason
            })
        }).then(() => {
            this.refresh();
        });
    }

    viewLeave(leave) {
        this.setState({
            managedLeave: leave
        });
    }

    back() {
        this.setState({
            managedLeave: undefined
        });
    }

    render() {
        return (
            <Jumbotron>
               <h1>Manger panel</h1> 
                {this.state.managedLeave === undefined ? (
                    <Table>
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Manage</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.leaveRequests && this.state.leaveRequests.map((request, index) => 
                                    <LeaveRequest key={index} data={request} view={this.viewLeave}/>
                            )}
                        </tbody>
                    </Table>
                ) :([
                    <ManageLeaveForm data={this.state.managedLeave} approve={this.approveLeave} reject={this.rejectLeave}/>,
                    <Button onClick={() => this.back()}>Back</Button>
                ])}
            </Jumbotron>
        )
    }
}

export default ManagerPanel;