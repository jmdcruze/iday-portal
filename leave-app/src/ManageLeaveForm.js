import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Jumbotron } from 'reactstrap';
import { InputGroup, InputGroupAddon, InputGroupText, Alert, Col, Row} from 'reactstrap';

class ManageLeaveForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rejectReason: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name] : value
        });
    }

    render() {
        const data = this.props.data;
        return (
            <Jumbotron className="leave-form">
                <Form>
                    <FormGroup row>
                        <Label>Username</Label>
                        <Col sm={10}>
                            <div>{data.requestor}</div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label>Reason</Label>
                        <Col sm={10}>
                            <div>{data.reason}</div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label>Type</Label>
                        <Col sm={10}>
                            <div>{data.type}</div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label>Start Date</Label>
                        <Col sm={10}>
                            <div>{data.startDate.split('T')[0]}</div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label>End Date</Label>
                        <Col sm={10}>
                            <div>{data.endDate.split('T')[0]}</div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label>Total Hours</Label>
                        <Col sm={10}>
                            <div>{data.totalhours}</div>
                        </Col>
                    </FormGroup>
                        {data.status === 'new' ? (
                            <Row>
                                <Col md={6}>
                                    <FormGroup>
                                        <Button color="primary" onClick={() => this.props.approve(data)}>Approve</Button>
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <InputGroup>
                                            <Input name="rejectReason" value={this.state.rejectReason} onChange={this.handleInputChange} placeholder="reason"/>
                                            <InputGroupAddon addonType="append">
                                                <Button color="warning" onClick={() => this.props.reject(data, this.state.rejectReason)}>Reject</Button>
                                            </InputGroupAddon>
                                        </InputGroup>
                                    </FormGroup>
                                </Col>
                            </Row>
                        ) : ([
                            <FormGroup row>
                                <Label>Comment</Label>
                                <Col sm={10}>
                                    <div>{data.updateComment}</div>
                                </Col>
                            </FormGroup>,
                            <FormGroup row>
                                <Label>Status</Label>
                                <Col sm={10}>
                                    <div>{data.status}</div>
                                </Col>
                            </FormGroup>
                        ])}
                  
                </Form>
            </Jumbotron>
        )
    }

}

export default ManageLeaveForm;