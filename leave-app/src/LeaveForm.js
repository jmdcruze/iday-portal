import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Jumbotron } from 'reactstrap';
import { InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';

const API_URL = process.env.REACT_APP_API_URL;

class LeaveForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            reason: '',
            type: 'annual',
            startDate: '',
            endDate: '',
            submitted: false,
            totalhours: 0,
            error: false
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name] : value
        });
    }

    handleSubmit(event) {
        fetch(API_URL + '/leaves',
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                requestor: this.state.username + '@booktopia.com.au',
                approver: 'justind@booktopia.com.au',
                type: this.state.type,
                reason: this.state.reason,
                totalHours: this.state.totalhours,
                startDate: this.state.startDate,
                endDate: this.state.endDate
            })
        }).then(() => {
            this.setState({
                submitted: true,
                error: false,
                username: '',
                type: 'annual',
                totalhours: 0,
                reason: '',
                startDate: '',
                endDate: ''
            });
        });
    }

    render() {
        return (
            <Jumbotron className="leave-form">
               <Form> 
                    <FormGroup>
                        <Label for="username">Username</Label>
                        <InputGroup>
                            <Input name="username" id="username" placeholder="username" value={this.state.username} onChange={this.handleInputChange}/>
                            <InputGroupAddon addonType="append">
                            <InputGroupText>@booktopia.com.au</InputGroupText>
                            </InputGroupAddon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <Label for="type">Leave type</Label>
                        <Input type="select" name="type" id="type" value={this.state.type} onChange={this.handleInputChange}>
                            <option value="annual">Annual</option>
                            <option value="sick">Sick</option>
                            <option value="carer">Carer</option>
                            <option value="feelings">Because I Feel Like It</option>
                            <option value="alien">Alien Abduction Therapy</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="startDate">Start Date</Label>
                        <Input type="date" name="startDate" id="startDate" value={this.state.startDate} onChange={this.handleInputChange}/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="endDate">End Date</Label>
                        <Input type="date" name="endDate" id="endDate" value={this.state.endDate} onChange={this.handleInputChange}/> 
                    </FormGroup>
                    <FormGroup>
                        <Label for="totalhours">Total Hours</Label>
                        <Input type="totalhours" name="totalhours" id="totalhours" value={this.state.totalhours} onChange={this.handleInputChange}/> 
                    </FormGroup>
                    <FormGroup>
                        <Label for="reason">Reason</Label>
                        <Input name="reason" id="reason" value={this.state.reason} onChange={this.handleInputChange}/> 
                    </FormGroup>
                    <FormGroup>
                        <Button onClick={() => this.handleSubmit()} color="success">Submit</Button>
                    </FormGroup>
                    {this.state.submitted && (
                        <FormGroup>
                            <Alert color="success">Leave sucessfully submitted</Alert>
                        </FormGroup>
                    )}
                    {this.state.error && (
                        <FormGroup>
                            <Alert color="danger">Oh noes! Some error happened that was bad.</Alert>
                        </FormGroup>
                    )}
                </Form>
            </Jumbotron>
        ) 
    }
}

export default LeaveForm;