import React from 'react';
import './App.css';
import ManagerPanel from './ManagerPanel';
import LeaveForm from './LeaveForm';
import { Container, Row, Col } from 'reactstrap';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import { TabContent, TabPane, Card, Button, CardTitle, CardText } from 'reactstrap';
import classnames from 'classnames';
    
class App extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
          activeTab: '1'
        };
    }


    toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }


    render() {
        return (
            <Container fluid>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Leave Application</NavbarBrand>
                </Navbar>
                <Nav tabs>
                    <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '1'})}
                        onClick={() => {this.toggle('1')}}
                        >
                            Leave Form
                        </NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink className={classnames({ active: this.state.activeTab === '2'})}
                        onClick={() => {this.toggle('2')}}
                        >
                            Manage Leave
                        </NavLink> 
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col xs="12"><LeaveForm/></Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="2">
                        <Row>
                            <Col xs="12"><ManagerPanel/></Col>
                        </Row>
                    </TabPane>
                </TabContent>
            </Container>
        ); 
    }
}

export default App;
