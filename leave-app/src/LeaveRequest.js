import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Jumbotron } from 'reactstrap';
import { InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';


class LeaveRequest extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const data = this.props.data; 
        return (
            <tr>
                <td>{data.requestor}</td>
                <td>{data.status}</td>
                <td>{data.startDate.split('T')[0]}</td>
                <td>{data.endDate.split('T')[0]}</td>
                <td><Button onClick={() => this.props.view(data)}>view</Button></td>
            </tr>
        )
    }

}

export default LeaveRequest;